extends Button

onready var sound = get_node("../../snd");
onready var label = get_node("../Label");

var files = []
export var path = "res://samples/kitt/"

func _ready():
	randomize()
	files = search_files(path,"ogg")


func _pressed():
	if !sound.playing:
		var snd_file = path + files[randi() % files.size()]
		#var snd_file = path + files[0]
		#label.text = snd_file
		snd_file = load(snd_file)
		sound.stream = snd_file
		sound.stream.loop = false
		sound.volume_db = 0
		sound.play()
	else:
		sound.stop()

func search_files(path,ext):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
 
	while true:
		var file = dir.get_next()
		file = file.replace('.import', '') # <--- remove the .import
		if file == "":
			break
		elif file.ends_with(ext):
			files.append(file)
	dir.list_dir_end()
	return files
