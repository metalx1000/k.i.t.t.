extends Button

onready var music = get_node("snd");

onready var tween_out = get_node("Tween")

export var transition_duration = 3.00
export var transition_type = 1 # TRANS_SINE

func _ready():
	pass # Replace with function body.

func _pressed():
	if !music.playing:
		music.volume_db = 0
		music.play()
	else:
		print("fade")
		fade_out(music)

func fade_out(stream_player):
	# tween music volume down to 0
	tween_out.interpolate_property(stream_player, "volume_db", 0, -80, transition_duration, transition_type, Tween.EASE_IN, 0)
	tween_out.start()
	# when the tween ends, the music will be stopped

func _on_Tween_tween_completed(object, key):
	object.stop()
	
